package com.example.hotspot;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yelp.parcelgen.Business;
import com.yelp.parcelgen.JsonUtil;

public class ResultsActivity extends Activity implements AnimationListener{
	public static final String EXTRA_BUSINESSES = "businesses";
	public static final String EXTRA_BUSINESS = "business";
	public static final String EXTRA_EVENTS = "events";
	public static final int DIALOG_PROGRESS = 42;
	
	private Yelp mYelp;
	private String searchTerm;
	private double latitude;
	private double longitude;
	private ArrayList<Business> Businesses;
	private BusinessArray bizArray;
	private DrawableManager drawableManager;
	private Business[] businessesRandArray;
	private int radius;
	
	RelativeLayout option1;
	RelativeLayout option2;
	RelativeLayout option3;
	ImageView loadingImage1;
	ImageView loadingImage2;
	ImageView loadingImage3;
	Button startAnimation;
	Animation anim;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_main);
		
		///Fragment fragment1 = new Fragment();
		//fragment1.onActivityCreated(savedInstanceState);
		drawableManager = new DrawableManager();
		// company stuff
		Businesses = getIntent().getParcelableArrayListExtra(EXTRA_BUSINESSES);
		getIntent().getParcelableArrayListExtra(EXTRA_EVENTS);
		bizArray = new BusinessArray(Businesses);
		if(MainActivity.getRadius()!=0)
		{
			radius = MainActivity.getRadius();
		}
		else
		{
			radius =1;
			MainActivity.setRadius(1);
    	}
		runAnimation();
		
	}
	private void runAnimation()
	{
		//generates the options
		generateOptions();
		//adds the clickers onto the hidden layouts
		addOnClicks();
		//sets the loading layouts to their corresponding layouts
		//RelativeLayout loadingLayout1 = (RelativeLayout) findViewById(R.id.option_loading_1);
		//RelativeLayout loadingLayout2 = (RelativeLayout) findViewById(R.id.option_loading_2);
		//RelativeLayout loadingLayout3 = (RelativeLayout) findViewById(R.id.option_loading_3);
		//sets the option layouts to their corresponding layouts
		option1 = (RelativeLayout) findViewById(R.id.option1);
		option2 = (RelativeLayout) findViewById(R.id.option2);
		option3 = (RelativeLayout) findViewById(R.id.option3);
		//creates a new value animator
		AnimationSet animSet = new AnimationSet(false);
		//making the options invisible
		option1.setVisibility(RelativeLayout.INVISIBLE);
		option2.setVisibility(RelativeLayout.INVISIBLE);
		option3.setVisibility(RelativeLayout.INVISIBLE);
		Animation anim1 = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.animation_fade_in);
		Animation anim2 = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.animation_fade_in);
		Animation anim3 = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.animation_fade_in);
		animSet.addAnimation(anim1);animSet.addAnimation(anim2);animSet.addAnimation(anim3);
		anim1.setDuration(500);
		anim2.setDuration(500);
		anim3.setDuration(500);
		anim2.setStartOffset(250);
		anim3.setStartOffset(450);
		option1.setAnimation(anim1);
		option2.setAnimation(anim2);
		option3.setAnimation(anim3);
		animSet.start();
	}
	private void generateOptions() {
		ImageView[] images = new ImageView[3];
		images[0] = (ImageView) findViewById(R.id.logo_1);
		images[1] = (ImageView) findViewById(R.id.logo_2);
		images[2] = (ImageView) findViewById(R.id.logo_3);
		TextView[] names = new TextView[3];
		names[0] = (TextView) findViewById(R.id.company_name_1);
		names[1] = (TextView) findViewById(R.id.company_name_2);
		names[2] = (TextView) findViewById(R.id.company_name_3);
		ImageButton[] ratings = new ImageButton[3];
		ratings[0] = (ImageButton) findViewById(R.id.rating_image_1);
		ratings[1] = (ImageButton) findViewById(R.id.rating_image_2);
		ratings[2] = (ImageButton) findViewById(R.id.rating_image_3);
		businessesRandArray = bizArray.threeRandomCompanies();
		int size = businessesRandArray.length;
		for (int x = 0; x < size; x++) {
			
			try{
				drawableManager.fetchDrawableOnThread(businessesRandArray[x].getImageUrl(),images[x]);
				drawableManager.fetchDrawableOnThread(businessesRandArray[x].getRatingImageUrl(),ratings[x]);
			}
			catch(NullPointerException e)
			{
				images[x]=null;
				ratings[x]=null;
			}
			names[x].setText(businessesRandArray[x].getName().subSequence(0,businessesRandArray[x].getName().length()));// for company
		}
	}
	public void search(View v)
    {
    	String terms = searchTerm;
    	Double newLatitude = new Double(latitude);
    	Double newLongitude = new Double(longitude);
    	String latitudeString = newLatitude.toString();
    	String longitudeString = newLongitude.toString();
    	
    	new AsyncTask<String, Void, ArrayList<Business>>() {
			@Override
			protected ArrayList<Business> doInBackground(String... params) {
				Double latitudeValue = new Double(params[1]);
				double latitudeVal = latitudeValue.doubleValue();
				Double longitudeValue = new Double(params[2]);
				double longitudeVal = longitudeValue.doubleValue();
				String result = mYelp.search(latitudeVal, longitudeVal, radius);
				try {
					JSONObject response = new JSONObject(result);
					if (response.has("businesses")) {
						return JsonUtil.parseJsonList(response.getJSONArray("businesses"), Business.CREATOR);
					}
				} catch (JSONException e) {
					return null;
				}
				return null;
			}
		
			@Override
			protected void onPostExecute(ArrayList<Business> businesses) {
				onSuccess(businesses);
			}
    		
    	}.execute(terms, latitudeString, longitudeString);
    }
    
    public void onSuccess(ArrayList<Business> businesses) {
    	// Launch BusinessesActivity with an intent that includes the received businesses
		if (businesses != null) {
			//intent.putParcelableArrayListExtra(BusinessesActivity.EXTRA_BUSINESSES, businesses);
			//startActivity(intent);
		} else {
			Toast.makeText(this, "An error occured during search", Toast.LENGTH_LONG).show();
		}
    }
    
    @Override
    public Dialog onCreateDialog(int id) {
    	if (id == DIALOG_PROGRESS) {
    		ProgressDialog dialog = new ProgressDialog(this);
    		dialog.setMessage("Loading...");
    		return dialog;
    	} else {
    		return null;
    	}
    }
	public void getSpecificBusiness(View view)
	{
		Business business = new Business();
		if(view.getId()==R.id.option_type_1)
		{
			business = businessesRandArray[0];
		}
		else if(view.getId()==R.id.option_type_2)
		{
			business = businessesRandArray[1];
		}
		else
		{
			business = businessesRandArray[2];
		}
		Intent intent = new Intent(this, IndividualResult.class);
		Parcelable[] array = new Parcelable[1];
		array[0] = business;
		intent.putExtra("business", array);
		startActivity(intent);
		/*
		TextView indieCompanyName = (TextView)findViewById(R.id.indie_company_name);
		TextView indieCompanyAddress = (TextView)findViewById(R.id.indie_company_address);
		//ImageView companyLogo = (ImageView)findViewById(R.id.indie_company_logo);
		indieCompanyName.setText(business.getName().subSequence(0,business.getName().length()));
		String address = "";
		for(String str: business.getLocation().getAddress())
		{
			address = address+str;
		}
		indieCompanyAddress.setText(address.subSequence(0, address.length()));
		*/
	}
	public void addOnClicks()
	{
		ImageButton[] buttons = new ImageButton[3];
		buttons[0] = (ImageButton)findViewById(R.id.option_type_1);
		buttons[1] = (ImageButton)findViewById(R.id.option_type_2);
		buttons[2] = (ImageButton)findViewById(R.id.option_type_3);
		for(ImageButton button: buttons)
		{
			button.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v) {
					getSpecificBusiness(v);
				}
				
			});
		}
	}
	public void restartActivity(View view)
    {
    	onCreate(null);
    }
	public void setRadiusView(View view)
	{
		Intent intent = new Intent(this, RadiusControllerView.class);
		//intent.putExtra("radius", MainActivity.RADIUS_NUM);
		startActivity(intent);
	}
	public void setHistoryView(View view)
	{
		Intent intent = new Intent(this, HistoryControllerView.class);
		//intent.putExtra("radius", MainActivity.RADIUS_NUM);
		startActivity(intent);
	}
	@Override
	public void onAnimationEnd(Animation animation) 
	{
		if (animation == anim) 
		{
			
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) 
	{
		
	}

	@Override
	public void onAnimationStart(Animation animation) 
	{
		
	}
}