package com.example.hotspot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
public class RadiusControllerView extends Activity{ 
	private EditText text;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.radius_view);	
		text = (EditText)findViewById(R.id.radius_number_id);
	}
	public void setNewRadius(View view)
	{
		//Log.v("EditText", mEdit.getText().toString());
		String radiusNum = text.getText().toString();
		int radiusSize = radiusNum.length();
		Integer integer = null;
		int finalInt;
		if(radiusSize != 0)
		{
			integer = new Integer(radiusNum);
			finalInt = integer.intValue();
		}
		else
		{
			finalInt = 0;
		}
		
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("radius", finalInt);
		startActivity(intent);
	}
}
