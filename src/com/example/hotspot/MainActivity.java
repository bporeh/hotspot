package com.example.hotspot;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yelp.parcelgen.Business;
import com.yelp.parcelgen.BusinessesActivity;
import com.yelp.parcelgen.JsonUtil;
//import android.os.Parcelable;
//import meetup.Client;
//import meetup.ClientSettings;
//import meetup.Event;
//import meetup.EventSearchCriteria;
//import meetup.Token;

public class MainActivity extends Activity implements AnimationListener {
	
	public static final int DIALOG_PROGRESS = 42;
	
//	private final String KEY ="bve6a5sg6tllb8itckm88j8vpd";
//    private final String SECRET = "q1avr72ua38lvo8gtmvenolnib";
//    private final String API_KEY = "1d6b6666dae185729x545716e607b";
    private ActionBar actionBar;
	private Yelp mYelp;
	private API_Static_Stuff staticStuff;
	private double latitude;
	private double longitude;
	private String searchTerm;
	private static int radius;
	
	
//	private Token token;
//	private ClientSettings cs;
//	private EventSearchCriteria esc;
//	private Client client;
//	
	ImageView logo;
	ImageView greenCircle;
	ImageView clickedLogo;
	TextView slogan;
	TextView clickHere;
	Animation anim;
	Animation rotationAnim;
	/** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);	
        //ANIMATION PRACTICE******************************************************************************
            	
        logo = (ImageView) findViewById(R.id.hotspot_button_get_started);
        slogan = (TextView) findViewById(R.id.let_us_plan);
        clickHere = (TextView) findViewById(R.id.touch_to_begin);
		runAnimation();        
		anim = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.animation_fade_in);
		anim.setAnimationListener(this);

		logo.startAnimation(anim);
		slogan.startAnimation(anim);
		clickHere.startAnimation(anim);
        //ANIMATION PRACTICE******************************************************************************
        actionBar = this.getActionBar();
		actionBar.show();
        staticStuff = new API_Static_Stuff();
        //setContentView(R.layout.main);
        mYelp = new Yelp(staticStuff.getYelpConsumerKey(), staticStuff.getYelpConsumerSecret(),
        		staticStuff.getYelpToken(), staticStuff.getYelpTokenSecret());
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		String locationProvider = LocationManager.NETWORK_PROVIDER;
		Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
		latitude = lastKnownLocation.getLatitude();
		longitude = lastKnownLocation.getLongitude();
		searchTerm = "";
		try{
			radius=this.getIntent().getIntExtra("radius",1);
		}catch(NullPointerException e)
		{
			radius=1;
		}
	}
    public void runAnimation()
    {
    	greenCircle = (ImageView) findViewById(R.id.green_wheel);
    	clickedLogo = (ImageView) findViewById(R.id.hotspot_button_get_started_clicked);
    	rotationAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.animation_rotate);
//    	AnimationSet animSet = new AnimationSet(false);
   // 	rotationAnim.setDuration(1000);
    	rotationAnim.setInterpolator(new LinearInterpolator());
//    	animSet.addAnimation(anim);
    	greenCircle.setAnimation(rotationAnim);
//    	animSet.start();
    	rotationAnim.start();
    }
    public void onClick(View v)
    {
    	changeAnimation();
    	search(v);
    }
    public void changeAnimation()
    {
    	rotationAnim.cancel();
       	clickedLogo.setVisibility(View.VISIBLE);
       	rotationAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.animation_rotate_ccw);
       	rotationAnim.setInterpolator(new LinearInterpolator());
       	greenCircle.setAnimation(rotationAnim);
       	rotationAnim.start();
    }
    public void search(View v)
    {
    	String terms = searchTerm;
    	Double newLatitude = new Double(latitude);
    	Double newLongitude = new Double(longitude);
    	String latitudeString = newLatitude.toString();
    	String longitudeString = newLongitude.toString();
    	
    	new AsyncTask<String, Void, ArrayList<Business>>() {
			@Override
			protected ArrayList<Business> doInBackground(String... params) {
				Double latitudeValue = new Double(params[1]);
				double latitudeVal = latitudeValue.doubleValue();
				Double longitudeValue = new Double(params[2]);
				double longitudeVal = longitudeValue.doubleValue();
				String result = mYelp.search(latitudeVal, longitudeVal, radius);
				try {
					JSONObject response = new JSONObject(result);
					if (response.has("businesses")) {
						return JsonUtil.parseJsonList(response.getJSONArray("businesses"), Business.CREATOR);
					}
				} catch (JSONException e) {
					return null;
				}
				return null;
			}
		
			@Override
			protected void onPostExecute(ArrayList<Business> businesses) {
				onSuccess(businesses);
			}
    		
    	}.execute(terms, latitudeString, longitudeString);
    }
    
    public void onSuccess(ArrayList<Business> businesses) {
    	// Launch BusinessesActivity with an intent that includes the received businesses
		if (businesses != null) {
			Intent intent = new Intent(MainActivity.this, ResultsActivity.class);
			intent.putParcelableArrayListExtra(BusinessesActivity.EXTRA_BUSINESSES, businesses);
			startActivity(intent);
		} else {
			Toast.makeText(this, "An error occured during search", Toast.LENGTH_LONG).show();
		}
    }
    
    @Override
    public Dialog onCreateDialog(int id) {
    	if (id == DIALOG_PROGRESS) {
    		ProgressDialog dialog = new ProgressDialog(this);
    		dialog.setMessage("Loading...");
    		return dialog;
    	} else {
    		return null;
    	}
    }
    @Override
	public void onAnimationEnd(Animation animation) 
	{
		if (animation == anim) 
		{
			
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) 
	{
		
	}

	@Override
	public void onAnimationStart(Animation animation) 
	{
		
	}
	public static int getRadius()
	{
		return radius;
	}
	public static void setRadius(int newRadius)
	{
		radius = newRadius;
	}
}