package com.example.hotspot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yelp.parcelgen.Business;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.ViewGroup;
//import android.view.View;

public class IndividualResult extends Activity
{
	public static final String EXTRA_BUSINESS = "business";
	
	protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
	     setContentView(R.layout.individualized_results);
	     Business indyBusiness = new Business();
	     try{
	    	 Intent currentIntent = this.getIntent();
	    	 Parcelable[] indyBusinessArray = currentIntent.getParcelableArrayExtra(EXTRA_BUSINESS);
	    	 indyBusiness = (Business)indyBusinessArray[0];
	     }catch(NullPointerException e)
	     {
	    	 e.printStackTrace();
	     }
	     ImageView logo = (ImageView)findViewById(R.id.indie_company_logo);
	     DrawableManager a = new DrawableManager();
	     try{
	    	 a.fetchDrawableOnThread(indyBusiness.getImageUrl(),logo);
	     }
	     catch(NullPointerException e)
	     {
	    	 logo = null;
	     }
	     TextView indieCompanyName = (TextView)findViewById(R.id.indie_company_name);
	     //ImageView companyLogo = (ImageView)findViewById(R.id.indie_company_logo);
	     indieCompanyName.setText(indyBusiness.getName().subSequence(0,indyBusiness.getName().length()));
	     String address = "";
	     for(String str: indyBusiness.getLocation().getAddress())
	     {
	    	 address = address+str;
	     }
	}
	public void doOnClick(View view)
	{
		((ImageView)findViewById(R.id.add_to_checklist)).setImageResource(R.drawable.check_orange);
		//Insert code that actually reserves stuff.
		this.finish();
	}
}