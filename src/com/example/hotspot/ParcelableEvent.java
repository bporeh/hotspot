package com.example.hotspot;

import java.math.BigDecimal;
import java.util.Calendar;

import meetup.Event;
import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableEvent extends Event implements Parcelable{

	protected String mLongitude;
	protected String mLatitude;
	protected Integer mRsvpCount;
	protected String mGroupName;
	protected String mFeeCurrency;
	protected Calendar mTime;
	protected Calendar mUpdated;
	protected Boolean mIsMeetup;
	protected String mEventUrl;
	protected Integer mAttendeeCount;
    protected String mId;
    protected String mVenueLongitude;
    protected String mVenueLatitude;
    protected BigDecimal mFee;
    protected String mVenueName;
    protected String mDescription;
    protected String mPhotoUrl;
    protected String mFeeDescription;
    protected String mQuestions;
    protected String mName;
	
    protected ParcelableEvent(Event event)
    {
    	mLongitude = event.getLongitude();
    	mLatitude = event.getLatitude();
    	mRsvpCount = event.getRsvpCount();
    	mGroupName = event.getGroupName();
    	mFeeCurrency = getFeeCurrency();
    	mTime = getTime();
    	mUpdated = getUpdated();
    	mIsMeetup = getIsMeetup();
    	mEventUrl = getEventUrl();
    	mAttendeeCount = getAttendeeCount();
        mId = getId();
        mVenueLongitude = getVenueLongitude();
        mVenueLatitude = getVenueLatitude();
        mFee = getFee();
        mVenueName = getVenueName();
        mDescription = getDescription();
        mPhotoUrl = getPhotoUrl();
        mFeeDescription = getFeeDescription();
        mQuestions = getQuestions();
        mName = getName();
    }
    protected ParcelableEvent(String longitude, String latitude, Integer rsvpCount, String groupName, String feeCurrency, Calendar time, Calendar updated, boolean isMeetup, String eventUrl, Integer attendeeCount, String id, String venueLongitude, String venueLatitude, BigDecimal fee, String venueName, String description, String photoUrl, String feeDescription, String questions, String name)
    {
    	mLongitude = longitude;
    	mLatitude = latitude;
    	mRsvpCount = rsvpCount;
    	mGroupName = groupName;
    	mFeeCurrency = feeCurrency;
    	mTime = time;
    	mUpdated = updated;
    	mIsMeetup = isMeetup  ;
    	mEventUrl = eventUrl;
    	mAttendeeCount = attendeeCount;
        mId = id;
        mVenueLongitude = venueLongitude;
        mVenueLatitude = venueLatitude;
        mFee = fee;
        mVenueName = venueName;
        mDescription = description;
        mPhotoUrl = photoUrl;
        mFeeDescription = feeDescription;
        mQuestions = questions;
        mName = name;
    }
    @Override
	
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		// TODO Auto-generated method stub
		boolean[] isMeetupArray = {getIsMeetup()};
		parcel.writeString(mLongitude);
		parcel.writeString(mLatitude);
		parcel.writeInt(mRsvpCount.intValue());
		parcel.writeString(mGroupName);
		parcel.writeString(mFeeCurrency);
		parcel.writeValue(mTime);
		parcel.writeValue(mUpdated);
		parcel.writeBooleanArray(isMeetupArray);
		parcel.writeString(mEventUrl);
		parcel.writeInt(mAttendeeCount);
		parcel.writeString(mId);
		parcel.writeString(mVenueLongitude);
		parcel.writeString(mVenueLatitude);
		parcel.writeValue(mFee);
		parcel.writeString(mVenueName);
		parcel.writeString(mDescription);
		parcel.writeString(mPhotoUrl);
		parcel.writeString(mFeeDescription);
		parcel.writeString(mQuestions);
		parcel.writeString(mName);
	}
	public Event getEvent()
	{
		return this;
	}

}
