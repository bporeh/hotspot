package com.example.hotspot;

import android.os.Parcel;
import android.os.Parcelable;

import com.yelp.parcelgen.Business;

public class Option implements Parcelable
{
//	protected double mLatitude;//Dont forget that in _Business it is Location and Meetup it is Longitude and Latitude
//	protected double mLongitude;
//	protected String mId;
//	protected String mName;
//	protected String mImageUrl;
//	protected String mDescription;
//	protected boolean mIsEvent;
//	protected int mReviewCount;
//	protected Integer mAttendeeCount;
	protected char mOptionType;
	protected ParcelableEvent mEvent;
	protected Business mBusiness;
	// either or depending on type. protected int mReviewCount; || protected Integer mAttendeeCount;

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		// TODO Auto-generated method stub
		char[] charArray = {mOptionType};
		parcel.writeCharArray(charArray);
		parcel.writeParcelable(mEvent, 0);
		parcel.writeParcelable(mBusiness, 0);
	}
	
	
}
