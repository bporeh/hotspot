package com.yelp.parcelgen;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;

import com.yelp.parcelgen.JsonParser.DualCreator;


public class Review extends _Review {

	public static final DualCreator<Review> CREATOR = new DualCreator<Review>() {

		public Review[] newArray(int size) {
			return new Review[size];
		}

		public Review createFromParcel(Parcel source) {
			Review object = new Review();
			object.readFromParcel(source);
			return object;
		}

		@Override
		public Review parse(JSONObject obj) throws JSONException {
			Review newInstance = new Review();
			newInstance.readFromJson(obj);
			return newInstance;
		}
	};

}