package com.yelp.parcelgen;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

abstract class _Review implements Parcelable {
	protected String mId;
	protected int mRating;
	protected String mRatingImageUrl;
	protected String mRatingImageSmallUrl;
	protected String mRatingImageLargeUrl;
	protected String mExcerpt;
	protected String mTimeCreated;
	protected _User mUser;

	protected _Review(String id, int rating, String ratingImageUrl, String ratingImageUrlSmall,String ratingImageUrlLarge, String excerpt, String timeCreated,_User user ) {
			mId = id;
			mRating = rating;
			mRatingImageUrl = ratingImageUrl;
			mRatingImageSmallUrl = ratingImageUrlSmall;
			mRatingImageLargeUrl = ratingImageUrlLarge;
			mExcerpt = excerpt;
			mTimeCreated = timeCreated;
			mUser = user;
		}

		protected _Review() {
			super();
		}
		public String getId() {
			 return mId;
		}
		public int rating() {
			 return mRating;
		}
		public String getRatingImageUrl() {
			 return mRatingImageUrl;
		}
		public String getRatingImageUrlSmall() {
			 return mRatingImageLargeUrl;
		}
		public String getRatingImageLargeUrl() {
			 return mRatingImageSmallUrl;
		}
		public String getExcerpt() {
			 return mExcerpt;
		}
		public String getTimeCreated() {
			 return mTimeCreated;
		}
		public _User getUser() {
			return mUser;
		}
		public int describeContents() {
			return 0;
		}

		public void writeToParcel(Parcel parcel, int flags) {
			parcel.writeParcelable(mUser,0);
			parcel.writeString(mId);
			parcel.writeString(mRatingImageUrl);
			parcel.writeString(mRatingImageLargeUrl);
			parcel.writeString(mRatingImageSmallUrl);
			parcel.writeString(mExcerpt);
			parcel.writeString(mTimeCreated);
		}
		public void readFromParcel(Parcel source) {
			mId = source.readString();
			mRating = source.readInt();
			mRatingImageUrl = source.readString();
			mRatingImageSmallUrl = source.readString();
			mRatingImageLargeUrl = source.readString();
			mExcerpt = source.readString();
			mTimeCreated = source.readString();
			mUser = source.readParcelable(_User.class.getClassLoader());
		}

		public void readFromJson(JSONObject json) throws JSONException {
			if (!json.isNull("user")) {
				mUser = User.CREATOR.parse(json.getJSONObject("user"));
			}
			if (!json.isNull("id")) {
				mId = json.optString("id");
			}
			if (!json.isNull("rating_img_url")) {
				mRatingImageUrl = json.optString("rating_img_url");
			}
			if (!json.isNull("rating_image_small_url")) {
				mRatingImageSmallUrl = json.optString("rating_img_url_small");
			}
			if (!json.isNull("rating_image_large_url")) {
				mRatingImageLargeUrl = json.optString("rating_image_large_url");
			}
			if (!json.isNull("excerpt")) {
				mExcerpt = json.optString("excerpt");
			}
			if (!json.isNull("time_created")) {
				mTimeCreated = json.optString("time_created");
			}
		}	
}