package com.yelp.parcelgen;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;

import com.yelp.parcelgen.JsonParser.DualCreator;


public class User extends _User {

	public static final DualCreator<User> CREATOR = new DualCreator<User>() {

		public User[] newArray(int size) {
			return new User[size];
		}

		public User createFromParcel(Parcel source) {
			User object = new User();
			object.readFromParcel(source);
			return object;
		}

		@Override
		public User parse(JSONObject obj) throws JSONException {
			User newInstance = new User();
			newInstance.readFromJson(obj);
			return newInstance;
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

}