package com.yelp.parcelgen;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;


abstract class _User implements Parcelable {
	protected String mId;
	protected String mImageUrl;
	protected String mName;
	
	protected _User(String id, String imageUrl, String name ) {
			mId = id;
			mImageUrl = imageUrl;
			mName = name;
		}
		protected _User() {
			super();
		}
		public String getId() {
			 return mId;
		}
		public String getImageUrl() {
			 return mImageUrl;
		}
		public String getName() {
			 return mName;
		}
		public void writeToParcel(Parcel parcel, int flags) {
			parcel.writeString(mId);
			parcel.writeString(mImageUrl);
			parcel.writeString(mName);
		}
		public void readFromParcel(Parcel source) {
			mId = source.readString();
			mImageUrl = source.readString();
			mName = source.readString();
		}

		public void readFromJson(JSONObject json) throws JSONException {
			if (!json.isNull("id")) {
				mId = json.optString("id");
			}
			if (!json.isNull("image_url")) {
				mImageUrl = json.optString("image_url");
			}
			if (!json.isNull("name")) {
				mName = json.optString("name");
			}
		}	
}